# KeplerEquation

Solving the Kepler-Equation to simulate our Solar-Planetary-System

### Quellen
  * [Kepler Gleichung Wikipedia](https://de.wikipedia.org/wiki/Kepler-Gleichung)

### Beschreibung

<img src="Image/KeplerEquationScheme.svg" alt="KeplerEquationScheme">
<img src="Image/KeplerGleichungLegende.png" alt="KeplerGleichungLegende">
<br><br>

$M$ : Mittlere Anomalie, Mittelpunkt Ellipse-$\measuredangle(-Planet,-Perihel)$ $\bigg\{[M] = 1\bigg\}$
$E$ : Exzentrische Anomalie, Hilfsgrösse $\bigg\{[E] = 1\bigg\}$
$T$ : Wahre Anomalie $\bigg\{[T] = 1\bigg\}$
$e$ : Numerische Exzentrizität der Ellipse $\bigg\{[e] = 1\bigg\}$
$U$ : Bahnperiode $\bigg\{[U] = s\bigg\}$
$a$ : Grosse Halbachse Ellipse $\bigg\{[a] = m\bigg\}$

Kepler Gleichung: $\boxed{M = E - e \cdot \sin E}$ $(1)$

Mittlere Anomalie $M$:
$\boxed{M = 2 \pi \dfrac{t - t_p}{U}}$ $(2)$

mit $t$ : Zeit $\bigg\{[t] = s\bigg\}$
mit $t_p$ : Zeitpunkt des Periheldurchgangs $\bigg\{[t_p] = s\bigg\}$

$\boxed{M = \omega_m (t - t_p)}$ $(3)$

mit
$\omega_m = \dfrac{2 \pi}{U}$ $\bigg\{[\omega_m] = \dfrac{1}{s}\bigg\}$


Wahre Anomalie $T$:
$\boxed{\tan \dfrac{T}{2} = \sqrt{\dfrac{1 - e}{1 + e}} \tan \dfrac{E}{2}}$ $(4)$
identisch
$\boxed{\cos T = \dfrac{\cos E - e}{1 - e \cos E}}$ $(5)$

Weiterhin:

$r$ : Fahrstrahl Sonne S - Planet P $\bigg\{[r] = m\bigg\}$

$\boxed{r = a - a e \cos{E}}$ $(6)$


### Bestimmung der Bahnkoordinaten

Mit $(2)$, Vorgabe $t_p$, $U$:

$M(t) = 2 \pi \dfrac{t - t_p}{U}$

Lösen der impliziten Gleichung $(1)$ mit $e$ nach $E(t)$:

$M(t) = E(t) - e \cdot \sin{E(t)}$

Aus $(6)$ mit $a$ für $r(t)$:

$\boxed{r(t) = a - a e \cos{E(t)}}$ $(7)$

und für $T$ (Fallunterscheidung!) mit $(5)$:

$\boxed{T = \arccos \bigg[ \dfrac{\cos E - e}{1 - e \cos E} \bigg]}$ $(8)$

Damit bestimmt sich der Radiusvektor $r = r(t) = r(T(t))$ in Polarkoordinaten mit $(7)$ und $(8)$.


### Lösung der Kepler-Gleichung mit Newton-Verfahren

Newton Iteration: $\boxed{x_{n+1} = x_{n} - \dfrac{f(x_n)}{\dfrac{df(x)}{dx} \bigg\vert_{x_n}}}$ $(9)$

Aus $(1)$ $M = E - e \cdot \sin E$ folgt:

$f(E) \coloneqq E - M - e \cdot \sin E = 0$

$\dfrac{df(E)}{dE} = \dfrac{d[E - M - e \cdot \sin E]}{dE} $

$\dfrac{df(E)}{dE} = 1 - e \cdot \cos E $

und damit die Iterationsvorschrift für $E_{n}$:

$\boxed{E_{n+1} = E_{n} - \dfrac{E_{n} - M - e \cdot \sin E_{n}}{1 - e \cdot \cos E_{n} }}$ $(10)$

(ebenso: [mathworld.wolfram.com/KeplersEquation](http://mathworld.wolfram.com/KeplersEquation.html))

### Bahndaten der Erde

Jahr..Perihel........................Entfernung..........Aphel....................Entfernung
2019	3. Januar 2019 06:19	147 099 760 km	5. Juli 2019 00:10	152 104 285 km
2020	5. Januar 2020 08:47	147 091 144 km	4. Juli 2020 13:34	152 095 295 km
2021	2. Januar 2021 14:50	147 093 163 km	6. Juli 2021 00:27	152 100 527 km
2022	4. Januar 2022 07:52	147 105 052 km	4. Juli 2022 09:10	152 098 455 km
2023	4. Januar 2023 17:17	147 098 925 km	6. Juli 2023 22:06	152 093 251 km

Für Erde 2019:
Periheldurchgang : $t_{pearth} = 3.3 d$ $[1 d = 24 h]$
Umlaufszeit : $U_{earth} = 365.25 d$
Exzentrizität : $e_{earth} = 0.0167086$
Grosse Halbachse : $a_{earth} = 1.017 AU$ $[1 AU = 1.496 \cdot 10^8 km]$

Python-Programm ergibt:

<img src="Image/OrbitEarth.png" alt="OrbitEarth">
