# KeplerEquation
#
import numpy as NP
import matplotlib.pyplot as PP
#
# Time to pass Perihel 
TPerihelEarth = 3.3 # [d]
#
# Time for complete Rotation
UEarth = 365.25 # [d] # -> [s] * 24 * 60 * 60
# 
# Excentricity (0 <= e < 1)
#EpsilonEarth = 0.0167086 # [1] 
#EpsilonEarth = 0.167086 # [1] 
EpsilonEarth = 0.5 # [1] 
#EpsilonEarth = 0.9 # [1] 
#
# Aphel MajorAxis a
AEarth = 1.017 # [AU] - Astronomical Units
#
def RadDeg(rad):
    return rad * 180.0 / NP.pi
#
def KeplerNewton(m, e):
    En = m # Start with M for En
    Enp = En
    # print("---")
    for I in range(0, 10, 1):
        Enp = En - (En - m - e * NP.sin(En)) / (1 - e * NP.cos(En))
        # print(Enp)
        if (NP.abs(En - Enp) < 1E-12):
            return Enp
        En = Enp
    return Enp
#
def KeplerApproximation(m, e):
    e2 = e * e
    e3 = e * e2
    En = m + (e - 0.125 * e3) * NP.sin(m) 
    En = En + 0.500 * e2 * NP.sin(2.0 * m)
    En = En + 0.375 * e3 * NP.sin(3.0 * m)
    return En
#
def RadiusOrbit(majoraxis, epsilon, excentricanomalie):
    return majoraxis * (1 - epsilon * NP.cos(excentricanomalie))
#
def AngleOrbit(epsilon, excentricanomalie):
    CosE = NP.cos(excentricanomalie)
    N = CosE - epsilon
    D = 1.0 - epsilon * CosE
    if (0 <= excentricanomalie) & (excentricanomalie < NP.pi):
        return NP.arccos(N / D)
    return -NP.arccos(N / D)
#
def PolarCartesian(a, r):
    X = r * NP.cos(a)
    Y = r * NP.sin(a)
    return [X, Y]
#    
# Main
#
# Calculation Earth : r(t) for one Year
VT = []
VM = []
VE = []
VRO = []
VAO = []
VXO = []
VYO = []
CountSteps = 56
for I in range(0, 1 + CountSteps, 1):
    # Time T
    T = UEarth * I / CountSteps
    # M : Mean Anomalie
    M = 2.0 * NP.pi * (T - TPerihelEarth) / UEarth
    # E : Excentric Anomalie
    E = KeplerNewton(M, EpsilonEarth)
    #E = KeplerApproximation(M, e)
    # RadiusOrbit : r(t)
    RO = RadiusOrbit(AEarth, EpsilonEarth, E)
    # AngleOrbit : T(t)
    AO = AngleOrbit(EpsilonEarth, E)
    #
    # print(T, T / UEarth, RadDeg(M), RadDeg(E), AO, RO)
    VT.append([T])
    VM.append([M])
    VE.append([E])
    VRO.append([RO])
    VAO.append([AO])
    [X, Y] = PolarCartesian(AO, RO)
    VXO.append(X)
    VYO.append(Y)
#
#PP.axis('equal')
#PP.plot(VT, VM)
#PP.plot(VT, VE)
#
#PP.plot(VT, VAO)
#PP.plot(VT, VRO)
#
#PolarPlot = PP.subplot(111, projection = 'polar')    
#PolarPlot.plot(VAO, VRO)
#
PP.axis('equal')
PP.plot(0, 0, "or")
PP.plot(VXO, VYO)
PP.plot(VXO, VYO, "ob")

    